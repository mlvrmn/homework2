﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{
    public class ListElement<T>
    {
        public T Data       { get; set; }
        public ListElement<T> Next  { get; set; }
       
        public ListElement(T data)
        {
            Data = data;
        }
    }
}

