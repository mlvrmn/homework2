﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{

    class Program
    {
        static void Main(string[] args)
        {
            CircularList<String> list = new CircularList<String>();

            for (Int32 i = 1; i < 10; i++)
            {
                String element = $"element number {i}";
                list.Add(element);
            }
            
            foreach (var element in list)
            {
                Console.WriteLine(element);
            }

            Console.ReadKey();
            Console.Clear();

            /*удаляю элементы с четным номером*/
            for (Int32 i = 0; i < 10; i += 2)
            {
                String element = $"element number {i}";
                list.Remove(element);
            }

            foreach (var element in list)
            {
                Console.WriteLine(element);
            }

            Console.ReadKey();

            list.Clear();
        }
    }
}
