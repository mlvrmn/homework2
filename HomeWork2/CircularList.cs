﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{
    public class CircularList<T> : IEnumerable, IAlgorithm<T>
    {
        public ListElement<T> Head  { get; private set; }
        public ListElement<T> Tail  { get; private set; }
        public Int32 Count       { get; private set; }
        public bool IsEmpty { get { return Count == 0; } }
       
        public void Add(T data)
        {
            ListElement<T> element = new ListElement<T>(data);

            if (Head == null)
            {
                Head = element;
                Tail = element;
                Tail.Next = Head;
            }
            else
            {
                element.Next = Head;
                Tail.Next = element;
                Tail = element;
            }
            Count++;
        }
        public Boolean Remove(T data)
        {
            ListElement<T> current = Head;
            ListElement<T> previous = null;

            if (IsEmpty) return false;

            do
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current == Tail)
                            Tail = previous;
                    }
                    else 
                    {
                        if (Count == 1)
                        {
                            Head = Tail = null;
                        }
                        else
                        {
                            Head = current.Next;
                            Tail.Next = current.Next;
                        }
                    }
                    Count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            } while (current != Head);

            return false;
        }
        public void Clear()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }
        public IEnumerator GetEnumerator()
        {
            ListElement<T> current = Head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != Head);
        }
    }
}









