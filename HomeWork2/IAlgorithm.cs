﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWork2
{
    interface IAlgorithm<T>
    {
        void Add(T element);
        Boolean Remove(T element);
        void Clear();
    }
}
